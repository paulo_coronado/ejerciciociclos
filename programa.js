/*
    Ejercicio: Dado un número a ingresado por el usuario:

        1. Imprimir los números desde -a hasta a
        2. Imprimir si a es par o impar
        3. Imprimir el número que sigue y el anterior

    5
    -5 -4 -3 -2 -1 0 1 2 3 4 5
    impar
    6 4
*/

let a;
let i;

a=prompt("Valor:");

//Convertir un string (texto) en su equivalente entero
a=parseInt(a);

i=-a;

while(i<=a){
    document.write(i+"<br>");
    i++;
}

if(a%2===0){

    document.write("Par<br>");

}else{

    document.write("Impar<br>");

}


document.write(a+1);
document.write("<br>");
document.write(a-1);



